local socket = require("socket")
local port = 33033
local timeout=5 -- seconds
local msglen=8
local fmtstr="%."..tostring(msglen).."u"

local server = assert(socket.bind("*", port))
print("Listening on port " .. port)

while true do
   local client = server:accept()
   print("connection from ", client:getpeername())
   client:settimeout(10)

   while true do
      local data, err = client:receive(msglen)
      if not err then
	 print("received", tonumber(data))
      elseif err=='timeout' then
	 print("no data in "..tostring(timeout).."s")
      elseif err=='closed' then
	 print("connection closed by peer.") 
	 break
      end
      client:send(string.format(fmtstr, data+1))
   end
   client:close()
end